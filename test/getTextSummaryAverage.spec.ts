import { assert } from "chai";
import { createReadStream } from "fs";
import { getTextSummaryAverage } from "../src/averages";


describe("getTextSummaryAverage()", () => {
  it("returns the correct average from a text-summary report when using integer coverage values", async () => {
    const avg = await getTextSummaryAverage(createReadStream("test/inputs/reports/correct1.txt"));

    assert.equal(avg, 62.5);
  });


  it("returns the correct average from a text-summary report when using float coverage values", async () => {
    const avg = await getTextSummaryAverage(createReadStream("test/inputs/reports/correct2.txt"));

    assert.equal(avg, 75.13);
  });


  it("throws an exception when an entry has an coverage value that is not a correct number", async () => {
    try {
      await getTextSummaryAverage(createReadStream("test/inputs/reports/incorrect1.txt"));

      assert.fail();
    } catch (error) {
      assert.match(error.message, /Missing or invalid entry: functions/);
    }
  });


  it("throws an exception when an entry has an coverage value that is not a number", async () => {
    try {
      await getTextSummaryAverage(createReadStream("test/inputs/reports/incorrect2.txt"));

      assert.fail();
    } catch (error) {
      assert.match(error.message, /Missing or invalid entry: lines/);
    }
  });


  it("throws an exception when missing a report entry", async () => {
    try {
      await getTextSummaryAverage(createReadStream("test/inputs/reports/incorrect3.txt"));

      assert.fail();
    } catch (error) {
      assert.match(error.message, /Missing or invalid entry: branches/);
    }
  });
});
