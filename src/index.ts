#! /usr/bin/env node


import chalk from "chalk";
import * as commander from "commander";
import * as packageJSON from "../package.json";
import { getTextSummaryAverage } from "./averages";
import { isFiniteNumber } from "./utils";


commander
  .name((packageJSON as any).name)
  .version((packageJSON as any).version)
  .option("-p, --pass", "Appends the calculated average to the end of the given stream")
  .option("-l, --limit <num>", "The limit below which this program will raise an error", Number)
  .parse(process.argv);


const LIMIT = isFiniteNumber(commander["limit"]) ? commander["limit"] : 0;
const PASS_THROUGH = Boolean(commander["pass"]);


/**
 * Wraps the given string in markings.
 */
const wrapMessage = (msg: string) => {
  const topHalfLength = Math.max(0, Math.floor((80 - commander.name().length) / 2));
  const topBar = `${"=".repeat(topHalfLength)} ${commander.name()} ${"=".repeat(topHalfLength)}`;

  return `${topBar}\n${msg}\n${"=".repeat(topBar.length)}`;
};


/**
 * Check whether the given average is below or above the limit.
 */
const checkAverage = (average: number) => {
  if (average < LIMIT) {
    throw new Error(`FAIL. Coverage average: ${average.toFixed(2)} is below the given limit: ${LIMIT}.`);
  }

  return `OK. Coverage average: ${average.toFixed(2)}.`;
};


/**
 * Outputs the success message and exits the program.
 */
const outputSuccess = (msg: string) => {
  console.log(chalk.green.bold(wrapMessage(msg)));
};


/**
 * Outputs the error message and exits the program.
 */
const outputError = (error: Error) => {
  console.error(chalk.red.bold(wrapMessage(error.message)));
};


/**
 * Exit if no data was passed into the program.
 */
if (process.stdin.isTTY) {
  outputError(new Error("You must pipe a text-summary report into this program."));
  process.exit(1);
}


/**
 * Output to stdout when the pass flag is set.
 */
if (PASS_THROUGH) {
  process.stdin.pipe(process.stdout);
}


getTextSummaryAverage(process.stdin)
  .then(checkAverage)
  .then(outputSuccess)
  .catch(error => {
    outputError(error);
    process.exitCode = 1;
  });
