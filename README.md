# nyc-text-summary-avg

[![Build status](https://gitlab.com/gLagiewka/nyc-text-summary-avg/badges/master/build.svg)](https://gitlab.com/gLagiewka/nyc-text-summary-avg/commits/master)
[![Coverage](https://gitlab.com/gLagiewka/nyc-text-summary-avg/badges/master/coverage.svg)](https://gLagiewka.gitlab.io/nyc-text-summary-avg)
[![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://gitlab.com/gLagiewka/nyc-text-summary-avg/blob/master/LICENSE.md)

Computes the average coverage from a nyc&#39;s text-summary report.

Use it in CI systems, like GitLab CI, that generate coverage by extracting it from job logs using regular expressions. It can also be used to stop the CI pipeline, when the coverage average is below the given limit.

---

## Installing 
 
When using yarn: 
```js
yarn add nyc-text-summary-avg 
``` 
 
When using npm: 
```js
npm install nyc-text-summary-avg
``` 

---

## Usage

The average is calculated from a stream piped from the output of the nyc command:
```js
nyc mocha | nyc-text-summary-avg
```

It relies on the text-summary report being present in the stream, so the text-summary reporter must be enabled in nyc's configuration options.

Either via the command line:
```js
nyc --reporter=text-summary mocha | nyc-text-summary-avg
```

or inside the package.json or .nycrc files:
```js
{
    "reporter": [
        "text-summary"
    ]
}
```

The output with the average is then printed to the console:
```js
============================== nyc-text-summary-avg ==============================
OK. Coverage average: 93.60.
==================================================================================
```

It can then be matched by this regular expression:
```js
^OK\. Coverage average:\s(100|(?:\d|[1-9]\d)(?:\.\d+)?).$
```

---

## Setting the limit
To set a limit below which this program will exit with an error, use the `-l` or `--limit` flag:
```js
nyc mocha | nyc-text-summary-avg -l 95
```

It then exits with an error code, that stops the CI pipeline:
```js
============================== nyc-text-summary-avg ==============================
FAIL. Coverage average: 93.60 is below the given limit: 95.
==================================================================================
```

---

## Passing through the original stream
By default, the output from the nyc command is consumed. To pass it through the program, use the `-p` or `--pass` flag:
```js
nyc mocha | nyc-text-summary-avg -p
```

---

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/gLagiewka/nyc-text-summary-avg/blob/master/LICENSE.md) file for details.
