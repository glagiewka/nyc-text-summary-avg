# [v1.0.1](https://gitlab.com/glagiewka/nyc-text-summary-avg/compare/v1.0.0...v1.0.1) (19-03-2018)

## Fixes (1 change)
- Fixed divide by zero bug in a helper function (92d11adaaef349a867859f1d9750ccbc3fcd6f9d)


# [v1.0.0](https://gitlab.com/glagiewka/nyc-text-summary-avg/compare/v0.1.0...v1.0.0) (07-02-2018)

## Improvements (1 change)
- Added check if the program has been started without an input stream (311ea2b0ffc5faff0da2df4d2f1e5256277c2418)
