import * as readline from "readline";
import { Readable } from "stream";
import { ownKeys, ownKeyValues } from "./utils";


/**
 * An error thrown during matching of the patterns.
 */
export class MatchError extends Error {
  constructor(msg: string, public readonly missingPattern: string) {
    super(msg);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}


/**
 * Reads the given stream line by line and tries to match the regular expressions defined in the patterns map.
 *
 * @param patterns  A map with regular expressions to match.
 * @param input     An input stream to read the lines from.
 * @return          A promise resolving with a map of results and rejecting when any of the regular expressions
 *  has not been matched.
 */
export const matchInLines = (
  patterns: { [index: string]: RegExp },
  input: Readable,
) => {
  const matchResults: { [index: string]: RegExpExecArray } = {};

  return new Promise<typeof matchResults>((resolve, reject) => {
    const onLine = (line: string) => {
      for (const [key, pattern] of ownKeyValues(patterns)) {
        if (!(key in matchResults)) {
          const match = pattern.exec(line);

          if (match) {
            matchResults[key] = match;
          }
        }
      }
    };

    const onClose = () => {
      for (const pattern of ownKeys(patterns)) {
        if (!(pattern in matchResults)) {
          return reject(new MatchError(`Missing '${pattern}' pattern.`, pattern));
        }
      }

      resolve(matchResults);
    };

    readline
      .createInterface({ input })
      .on("line", onLine)
      .once("close", onClose);
  });
};
