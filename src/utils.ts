export function avg<TValue extends number>(iterable: Iterable<TValue>) {
  let sum = 0;
  let count = 0;

  for (const number of iterable) {
    sum += number;
    count += 1;
  }

  return count ? sum / count : 0;
}


export const isFiniteNumber = (value: any): value is number => {
  return typeof value === "number" && isFinite(value);
};


export function* map<TValue, TYieldValue>(
  iterable: Iterable<TValue>,
  callback: (value: TValue) => TYieldValue,
): Iterable<TYieldValue> {
  for (const value of iterable) {
    yield callback(value);
  }
}


export function* ownKeys(obj: any) {
  for (const key in Object(obj)) {
    if (Object.hasOwnProperty.call(obj, key)) {
      yield key;
    }
  }
}


export function* ownKeyValues<TValue extends { [index: string]: any }>(
  obj: TValue,
): IterableIterator<[string, typeof obj[keyof TValue]]> {
  yield* map(ownKeys(obj), (key): [string, any] => [key, obj[key]]);
}


export function* ownValues<TValue extends { [index: string]: any }>(
  obj: TValue,
): IterableIterator<typeof obj[keyof TValue]> {
  yield* map(ownKeys(obj), (key) => obj[key]);
}
