import { assert } from "chai";
import { createReadStream } from "fs";
import { MatchError, matchInLines } from "../src/matcher";


describe("matchInLines()", () => {
  it("resolves with an empty map when given an empty pattern map", async () => {
    const matchResults = await matchInLines({}, createReadStream("test/inputs/text.txt"));

    assert.isEmpty(matchResults);
  });


  it("resolves with an empty map when given an empty pattern map and an empty input stream", async () => {
    const matchResults = await matchInLines({}, createReadStream("test/inputs/empty.txt"));

    assert.isEmpty(matchResults);
  });


  it("resolves with a map of results when a pattern has been matched", async () => {
    const textToMatch = "It was popularised in the 1960s";
    const matchResults = await matchInLines({ p1: new RegExp(textToMatch) }, createReadStream("test/inputs/text.txt"));

    assert.hasAllKeys(matchResults, ["p1"]);
    assert.equal(matchResults.p1[0], textToMatch);
  });


  it("resolves with a map of results when patterns have been matched", async () => {
    const textToMatch = "It was popularised in the (\\d{4})";
    const textToMatch2 = "Aldus PageMaker";
    const matchResults = await matchInLines({
      p1: new RegExp(textToMatch),
      p2: new RegExp(textToMatch2),
    }, createReadStream("test/inputs/text.txt"));

    assert.hasAllKeys(matchResults, ["p1", "p2"]);
    assert.equal(matchResults.p1[1], "1960");
    assert.equal(matchResults.p2[0], textToMatch2);
  });


  it("rejects when a pattern has not been matched", async () => {
    try {
      await matchInLines(
        { p1: /Some text that doesn't exist inside the test text/ },
        createReadStream("test/inputs/text.txt"),
      );

      assert.fail();
    } catch (error) {
      assert.instanceOf(error, MatchError);
      assert.propertyVal(error, "missingPattern", "p1");
    }
  });
});
