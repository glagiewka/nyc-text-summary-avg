import { Readable } from "stream";
import { MatchError, matchInLines } from "./matcher";
import { avg, isFiniteNumber, ownValues } from "./utils";


/**
 * Calculates an average coverage from the stream containing a nyc text-summary report.
 *
 * @param input   A readable stream containing the report.
 * @return        A promise resolving with the average and rejecting when one of the summary's entries is missing.
 */
export const getTextSummaryAverage = (input: Readable) => {
  const patterns = {
    branches: new RegExp(/Branches\s*:\s*(100|(?:\d|[1-9]\d)(?:\.\d+)?)%/),
    functions: new RegExp(/Functions\s*:\s*(100|(?:\d|[1-9]\d)(?:\.\d+)?)%/),
    lines: new RegExp(/Lines\s*:\s*(100|(?:\d|[1-9]\d)(?:\.\d+)?)%/),
    statements: new RegExp(/Statements\s*:\s*(100|(?:\d|[1-9]\d)(?:\.\d+)?)%/),
  };

  return matchInLines(patterns, input)
    .then(matches => {
      const coveragePercentages = Array
        .from(ownValues(matches))
        .map(match => Number(match[1]))
        .filter(isFiniteNumber);

      return avg(coveragePercentages);
    })
    .catch((error: Error) => {
      if (error instanceof MatchError) {
        throw new Error(
          `No valid nyc text-summary report was found. Missing or invalid entry: ${error.missingPattern}.`,
        );
      }

      throw error;
    });
};
